#ifndef SECONDLAYER_H
#define SECONDLAYER_H
#include <cstdint>
#include <string>
#include <iostream>
#include <vector>


#include "firstlayer.hpp"

class SecondDS
{
public:
    /**
    *   @brief  public constructor
    */
    SecondDS(uint32_t id, std::string msg, u_int16_t bool_and_cnt, uint8_t opts):
        id(id), message(msg)
    {
        set_bool_cnt(bool_and_cnt);
        set_opts(opts);
    }

    /**
    *   @brief  prints out the contents of the object
    */
    void print()
    {
        std::cout << "id = " << id;
        std::cout << ", msg = " << message;
        std::cout << " , bool = " << (aliens_exist ? "true" : "false") << " , count = " << count;
        std::cout << " , opts = { " << static_cast<int>(option1) << " "<< static_cast<int>(option2);
        std::cout << " " << static_cast<int>(option3) << " " << static_cast<int>(option4) << " }\n";
    }

private:
    uint32_t id;
    std::string message;
    bool aliens_exist;
    int16_t count;
    uint8_t option1;
    uint8_t option2;
    uint8_t option3;
    uint8_t option4;


    /**
    *   @brief  masks out an 1 bit boolean and a 15 bit signed integer
    */
    void set_bool_cnt(uint16_t data)
    {
        this->aliens_exist = data & (1<<15);
        uint16_t mask = static_cast<uint16_t>(~(1<<15));
        int16_t val = static_cast<int16_t>(((data & mask) << 1));
        val /= 2;
        this->count = val;

    }

    /**
    *   @brief  masks out four 2 bit options from 8 bits
    */
    void set_opts(u_int8_t data)
    {
        option1 = (data & (0x3 << 6)) >> 6;
        option2 = (data & (0x3 << 4)) >> 4;
        option3 = (data & (0x3 << 2)) >> 2;
        option4 = data & 0x3;
    }
};



/**
*   @brief  reads vector of BLOB data
*
*   @param BLOBdata is the input vector to generate
*   @param output_vec parameter for storing the parsed data
*
*/
void parse_BLOB_data(const std::vector<FirstDS>& BLOBdata, std::vector<SecondDS>& output_vec);

#endif



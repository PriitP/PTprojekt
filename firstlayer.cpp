#include <string>
#include <iostream>

#include "firstlayer.hpp"


/**
*   @brief local function for parsing a line
*
*   @param line is the input data to be parsed
*   @param output_data is for storing the result
*   @return true if data could be parsed
*/
static bool parse_line(const std::string& line, std::vector<FirstDS>& output_data);


bool fill_BLOB_vec(std::ifstream &ifs, std::vector<FirstDS> &vec)
{
    std::string line;
    while(std::getline(ifs, line))
    {
        bool ok = parse_line(line, vec);
        if(!ok)
        {
            // faili sisuga probleeme
            return false;
        }
    }
    return true;
}



static bool parse_line(const std::string& line, std::vector<FirstDS>& output_data)
{
    char* ptr = nullptr;
    const char * parse_ptr = nullptr;
    uint32_t adr = static_cast<uint32_t>(strtoul(line.c_str(), &ptr, 16)); // vaikimisi eeldus, et adr ei ole kunagi suurem kui 32 bitti
    if(ptr != nullptr && ptr != line.c_str() && *ptr == ':')
    {
        // adr on leitud, reaga saab edasi minna
        parse_ptr = ptr + 1;
    }
    else
    {
        return false;
    }
    const char* end_ptr = &(*line.end());
    char hex_char1 = 0;
    char hex_char2 = 0;
    char buf[3] = {0};

    // algab ülejaanud osa reast läbikäimine
    while(parse_ptr != end_ptr)
    {
        char c = *parse_ptr;
        switch (c)
        {
            case ' ':
                parse_ptr++;
                break;
            default:
                if(isxdigit(c))
                {
                    if(hex_char1 == 0)
                    {
                        //esimene hex ASCII märk leitud
                        hex_char1 = c;
                        parse_ptr++;
                    }
                    else
                    {
                        // teine hex ASCII märk leitud, lisame selle vectorisse
                        hex_char2 = c;
                        buf[0] = hex_char1;
                        buf[1] = hex_char2;
                        uint8_t hex_num = static_cast<uint8_t>(strtoul(buf, nullptr, 16));
                        if(output_data.empty()|| output_data.back().valid_count == DATA_ARRAY_SIZE)
                        {
                            output_data.push_back(FirstDS());
                            output_data.back().adr = adr;
                            output_data.back().valid_count = 0;
                        }
                        FirstDS& back_ref = output_data.back();
                        back_ref.data[back_ref.valid_count] = hex_num;
                        back_ref.valid_count++;
                        hex_char1 = hex_char2 = buf[0] = buf[1] = 0;
                        parse_ptr++;

                    }
                }
                else
                {
                    // jõudsime reas kohta, kus ei ole hex ASCII märki ega tühikut, seega on rida läbi
                    parse_ptr = end_ptr;
                }
                break;
        }

    }
    return true;
}

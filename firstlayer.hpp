#ifndef FIRSTLAYER_H
#define FIRSTLAYER_H
#include <fstream>
#include <vector>
#include <array>
#include <cstdint>


const size_t DATA_ARRAY_SIZE = 16;

struct FirstDS
{
    uint32_t adr;
    size_t valid_count;
    std::array<uint8_t, DATA_ARRAY_SIZE> data;
};




/**
*   @brief  reads file stream to generate a vector of BLOB data
*
*   @param line is the input data to be parsed
*   @param output_data is for storing the result
*   @return true if file was parsed fine, false if it failed
*/
bool fill_BLOB_vec(std::ifstream& ifs, std::vector<FirstDS>& vec);

#endif

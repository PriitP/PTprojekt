#include "secondlayer.hpp"


/**
*   @brief  local helper function to get the next byte from BLOB vector
*
*   @param BLOBdata input vector from where data is extracted
*   @param i current position in the vector
*   @param j current position in the FirstDS array
*   @return extracted byte
*
*/
static uint8_t next_byte(const std::vector<FirstDS>& BLOBdata, size_t& i, size_t& j);


/**
*   @brief  local helper currently read byte count
*
*   @param i current position in the vector
*   @param j current position in the FirstDS array
*   @return total count
*
*/
static size_t bytes_read(size_t i, size_t j);



void parse_BLOB_data(const std::vector<FirstDS>& BLOBdata, std::vector<SecondDS>& output_vec)
{
    if(BLOBdata.empty())
    {
        // pole midagi teha
        return;
    }
    const size_t byte_count = (BLOBdata.size() - 1) * DATA_ARRAY_SIZE + BLOBdata.back().valid_count;
    size_t i = 0;
    size_t j = 0;
    while(true)
    {
        if((bytes_read(i, j) + 7) > byte_count)
        {
            // liiga vähe baite, et midagi edasi teha
            return;
        }

        // id leidmine
        uint32_t b0 = next_byte(BLOBdata, i, j);
        uint32_t b1 = next_byte(BLOBdata, i, j);
        uint32_t b2 = next_byte(BLOBdata, i, j);
        uint32_t b3 = next_byte(BLOBdata, i, j);
        uint32_t id = (b0 << 24) | (b1 << 16) | (b2 << 8) | (b3);
        std::string msg;

        // c string leidmine ja sellest std::stringi ehitamine

        while(true)
        {
            uint8_t b = next_byte(BLOBdata, i, j);
            if(b == 0)
            {
                break;
            }
            else if(bytes_read(i, j) == byte_count)
            {
                return;
            }
            else
            {
                msg += static_cast<char>(b);
            }
        }
        if((bytes_read(i, j) +  3) > byte_count)
        {
            return;
        }
        else
        {
            // viiamse kolme baiti leidmine, st bool, 15bit int ja 4 opti
            uint16_t high_byte = next_byte(BLOBdata, i, j);
            uint16_t low_byte = next_byte(BLOBdata, i, j);
            uint16_t bool_and_count = static_cast<uint16_t>((high_byte << 8) | low_byte);
            uint8_t opts = next_byte(BLOBdata, i, j);
            output_vec.emplace_back(SecondDS(id, msg, bool_and_count, opts));
        }
    }
}

static uint8_t next_byte(const std::vector<FirstDS>& BLOBdata, size_t& i, size_t& j)
{
    uint8_t ret = BLOBdata[i].data[j];
    j++;
    if(j == DATA_ARRAY_SIZE)
    {
        i++;
        j = 0;
    }
    return ret;
}

static size_t bytes_read(size_t i, size_t j)
{
    return (i * DATA_ARRAY_SIZE) + j;
}

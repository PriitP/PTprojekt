#include <iostream>
#include <fstream>
#include <vector>

#include "firstlayer.hpp"
#include "secondlayer.hpp"



/**
*   @brief program entry point
*
*/
int main(int argc, char *argv[])
{
    const int PROGRAM_OK = 0;
    const int PROGRAM_FAIL = 1;
    if(argc != 2)
    {
        std::cout << "Pole piisav arv argumente!\n";
        return PROGRAM_FAIL;
    }
    else
    {
        // proovi avada fail käsurealt tulnud nimega
        std::ifstream ifs(argv[1]);
        if(ifs.is_open())
        {
            std::vector<FirstDS> BLOB_vec;

            // loe failist sisu ja loo esimene vector

            bool ok = fill_BLOB_vec(ifs, BLOB_vec);

            if(ok)
            {
                // loe eelnevalt saadud andmestruktuurist järgmine vector
                std::vector<SecondDS> DS_vec;
                parse_BLOB_data(BLOB_vec, DS_vec);

                // Prindi saadud sisu välja!
                for(SecondDS& t : DS_vec)
                {
                    t.print();
                }
                std::cout << "\n\nKokku oli " << DS_vec.size() << " elementi teises kihis!\n";
                return PROGRAM_OK;
            }
            else
            {
                std::cout << "Faili sisuga probleem!\n";
                return PROGRAM_FAIL;
            }
        }
        else
        {
            std::cout << "Fail = " << argv[1] << " ei avanenud!\n";
            return PROGRAM_FAIL;
        }
    }
}
